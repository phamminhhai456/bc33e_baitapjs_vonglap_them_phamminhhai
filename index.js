function inSoNguyenTo() {
  var n = document.getElementById("soNhap").value * 1;
  var daySo = "";
  for (var i = 2; i <= n; i++) {
    if (kiemTraSoNguyenTo(i)) {
      daySo += i + " ";
    }
  }
  document.getElementById("ketQua").innerHTML = daySo;
}

function kiemTraSoNguyenTo(n) {
  if (n < 2) {
    return false;
  }
  var canBacHai = Math.sqrt(n);
  for (var i = 2; i <= canBacHai; i++) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}
